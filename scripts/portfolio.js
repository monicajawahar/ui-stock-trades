/**
 *
 */
var username = window.localStorage.getItem("username");
function populatePortfolioTable() {
  fetch(url + "api/portfolio/username=" + username) //promise object to return data from Rest API
    .then((response) => response.json())
    .then((Stocks) => {
      console.log(Stocks);
      var temp = "";
      if (Stocks.length > 0) {
        Stocks.forEach((itemData) => {
          temp += "<tr>";
          temp += "<td>" + itemData.stockTicker + "</td>";
          temp += "<td>" + itemData.volume + "</td>";
          temp +=
            "<td><a href='ordernow.html'><button class='btn btn-success mx-2 mt-1'>Buy</button></a><a href='ordernow.html'><button class='btn btn-danger mx-2 mt-1'>Sell</button></a></td></tr>";
        });
        document.getElementById("tbodyStocks").innerHTML = temp; //populate the html element with the ID of  tbodyStocks with TR tags
      }
    });
}

function getListOfTradesToUpdateInPortfolio() {
  fetch(url + "api/stock-trades/username=" + username)
    .then((response) => response.json())
    .then((trades) => {
      if (trades.length > 0) {
        trades.forEach((itemData) => {
          if (itemData.inPortfolio == 0) {
            if (itemData.statusCode == 2) {
              if (itemData.username == username) {
                updatePortfolio(itemData);
                const dataPut = {
                  id: itemData.id,
                  buyOrSell: itemData.buyOrSell,
                  nameOfStock: itemData.nameOfStock,
                  price: itemData.price,
                  statusCode: itemData.statusCode,
                  stockTicker: itemData.stockTicker,
                  volume: itemData.volume,
                  username: itemData.username,
                  date_time: itemData.date_time,
                  inPortfolio: 1,
                };
                fetch(url + "api/stock-trades/", {
                  method: "PUT",
                  headers: {
                    "Content-Type": "application/json",
                  },
                  body: JSON.stringify(dataPut),
                })
                  .then((response) => response.json())
                  .catch((error) => {
                    console.error("Error:", error);
                  });
              }
            }
          }
        });
      }
      populatePortfolioTable();
    });
}

async function updatePortfolio(data) {
  var checkInPortfolio = 0;
  await fetch(url + "api/portfolio/username=" + username)
    .then((response) => response.json())
    .then((trades) => {
      if (trades.length > 0) {
        trades.forEach((itemData) => {
          if (itemData.stockTicker == data.stockTicker) {
            checkInPortfolio = itemData.id;
          }
        });
      }
      if (checkInPortfolio != 0) {
        fetch(url + "api/portfolio/" + checkInPortfolio)
          .then((response) => response.json())
          .then((trades) => {
            if (data.buyOrSell == "Buy") {
              const dataPut = {
                id: trades.id,
                statusCode: trades.statusCode,
                stockTicker: trades.stockTicker,
                username: trades.username,
                volume: trades.volume + data.volume,
              };
              fetch(url + "api/portfolio/", {
                method: "PUT",
                headers: {
                  "Content-Type": "application/json",
                },
                body: JSON.stringify(dataPut),
              })
                .then((response) => response.json())
                .catch((error) => {
                  console.error("Error:", error);
                });
            } else if (data.buyOrSell == "Sell") {
              if (trades.volume >= data.volume) {
                const dataPut = {
                  id: trades.id,
                  statusCode: trades.statusCode,
                  stockTicker: trades.stockTicker,
                  username: trades.username,
                  volume: trades.volume - data.volume,
                };
                fetch(url + "api/portfolio/", {
                  method: "PUT",
                  headers: {
                    "Content-Type": "application/json",
                  },
                  body: JSON.stringify(dataPut),
                })
                  .then((response) => response.json())
                  .catch((error) => {
                    console.error("Error:", error);
                  });
              }
            }
          });
      } else {
        const dataPut = {
          id: data.id,
          statusCode: data.statusCode,
          stockTicker: data.stockTicker,
          username: data.username,
          volume: data.volume,
        };

        fetch(url + "api/portfolio/", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(dataPut),
        })
          .then((response) => response.json())
          .catch((error) => {
            console.error("Error:", error);
          });
      }
    });
}
