var listOfStocks = [];
var username = window.localStorage.getItem("username");
function increaseValue() {
  var value = parseInt(document.getElementById("quantityFetch").value, 10);
  value = isNaN(value) ? 0 : value;
  value++;
  document.getElementById("quantityFetch").value = value;
  document.getElementById("tradeValueReadOnly").value =
    parseFloat(document.getElementById("stockPriceReadOnly").value) *
    parseFloat(document.getElementById("quantityFetch").value);
}

function decreaseValue() {
  var value = parseInt(document.getElementById("quantityFetch").value, 10);
  value = isNaN(value) ? 0 : value;
  value < 1 ? (value = 1) : "";
  value--;
  document.getElementById("quantityFetch").value = value;
  document.getElementById("tradeValueReadOnly").value =
    parseFloat(document.getElementById("stockPriceReadOnly").value) *
    parseFloat(document.getElementById("quantityFetch").value);
}
function checkSellPossible(stockTkr, sellQty) {}
function getListOfStocksForPlacingOrder() {
  listOfStocks = [];
  fetch(url + "api/stock-list/")
    .then((response) => response.json())
    .then((trades) => {
      if (trades.length > 0) {
        trades.forEach((itemData) => {
          listOfStocks.push(itemData.nameOfStock);
        });
      }
    });
  autocompleteOrderList(
    document.getElementById("stockNameFetch"),
    listOfStocks
  );
}

function autocompleteOrderList(inp, arr) {
  var currentFocus;
  inp.addEventListener("input", function (e) {
    var a,
      b,
      i,
      val = this.value;
    closeAllLists();
    if (!val) {
      return false;
    }
    currentFocus = -1;
    a = document.createElement("DIV");
    a.setAttribute("id", this.id + "autocomplete-list");
    a.setAttribute("class", "autocomplete-items");
    this.parentNode.appendChild(a);
    for (i = 0; i < arr.length; i++) {
      if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
        b = document.createElement("DIV");

        b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
        b.innerHTML += arr[i].substr(val.length);

        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";

        b.addEventListener("click", function (e) {
          inp.value = this.getElementsByTagName("input")[0].value;

          closeAllLists();
        });
        a.appendChild(b);
      }
    }
  });

  function closeAllLists(elmnt) {
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }

  document.addEventListener("click", function (e) {
    closeAllLists(e.target);
  });
}

function getPriceOfStock() {
  var nameOfStockSearched = document.getElementById("stockNameFetch").value;
  var count = 0;
  for (var i = 0; i < listOfStocks.length; i++) {
    if (listOfStocks[i] == nameOfStockSearched) {
      count++;
    }
  }
  if (count > 0) {
    fetch(url + "api/stock-list/nameOfStock=" + nameOfStockSearched)
      .then((response) => response.json())
      .then((stockDetails) => {
        document.getElementById("stockPriceReadOnly").value =
          stockDetails.price;
      });
  } else {
    alert("The entered stock is not available");
  }
}

function verifyQuantity() {
  if (parseInt(document.getElementById("quantityFetch").value) <= 0) {
    return false;
  }
  return true;
}
function placeStockOrder() {
  var bOrS = "";
  if (document.getElementById("inlineRadio1").checked) {
    bOrS = "Buy";
  } else if (document.getElementById("inlineRadio2").checked) {
    bOrS = "Sell";
  } else {
    alert("Choose action");
  }

  var userName = "";
  var today = new Date();
  var dd = String(today.getDate()).padStart(2, "0");
  var mm = String(today.getMonth() + 1).padStart(2, "0");
  var yyyy = today.getFullYear();
  var hh = String(today.getHours()).padStart(2, "0");
  var min = String(today.getMinutes()).padStart(2, "0");
  var ss = String(today.getSeconds()).padStart(2, "0");
  today = yyyy + "-" + mm + "-" + dd + " " + hh + ":" + min + ":" + ss;
  const nm = document.getElementById("stockNameFetch").value;
  fetch(url + "api/stock-list/nameOfStock=" + nm)
    .then((response) => response.json())
    .then((stockDetails) => {
      var checkQuantity = verifyQuantity();
      if (checkQuantity) {
        if (bOrS == "Sell") {
          fetch(url + "api/portfolio/username=" + username) //promise object to return data from Rest API
            .then((response) => response.json())
            .then((Stocks) => {
              console.log(Stocks);
              if (Stocks.length > 0) {
                Stocks.forEach((itemData) => {
                  if (itemData.stockTicker == stockDetails.stockTicker) {
                    if (
                      itemData.volume >=
                      parseInt(document.getElementById("quantityFetch").value)
                    ) {
                      const data1 = {
                        buyOrSell: bOrS,
                        date_time: today,
                        id: 0,
                        inPortfolio: 0,
                        nameOfStock: document.getElementById("stockNameFetch")
                          .value,
                        price: parseInt(
                          document.getElementById("stockPriceReadOnly").value
                        ),
                        statusCode: 0,
                        stockTicker: stockDetails.stockTicker,
                        username: username,
                        volume: parseInt(
                          document.getElementById("quantityFetch").value
                        ),
                      };
                      fetch(url + "api/stock-trades/", {
                        method: "POST",
                        headers: {
                          "Content-Type": "application/json",
                        },
                        body: JSON.stringify(data1),
                      })
                        .then((response) => response.json())
                        .then((data) => {
                          console.log("Success:", data);
                          alert("Your Order has been placed");
                        })
                        .catch((error) => {
                          console.error("Error:", error);
                        });
                    } else {
                      alert(
                        "Available " +
                          itemData.stockTicker +
                          " stocks: " +
                          itemData.volume
                      );
                    }
                  } else {
                    alert("Stock not available in your portfolio");
                  }
                });
              } else {
                alert("Stock not available in your portfolio");
              }
            });
        } else if (bOrS == "Buy") {
          const data1 = {
            buyOrSell: bOrS,
            date_time: today,
            id: 0,
            inPortfolio: 0,
            nameOfStock: document.getElementById("stockNameFetch").value,
            price: parseInt(
              document.getElementById("stockPriceReadOnly").value
            ),
            statusCode: 0,
            stockTicker: stockDetails.stockTicker,
            username: username,
            volume: parseInt(document.getElementById("quantityFetch").value),
          };
          fetch(url + "api/stock-trades/", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data1),
          })
            .then((response) => response.json())
            .then((data) => {
              console.log("Success:", data);
              alert("Your Order has been placed");
            })
            .catch((error) => {
              console.error("Error:", error);
            });
        }
      }
    });
}

const QueryString = window.location.search;
console.log(QueryString);
