//NAVBAR
var username = window.localStorage.getItem("username");
var listOfStocks = [];

//Nav bar search button click - displays modal
function getStockDetailsBasedOnSearch() {
  var nameOfStockSearched = document.getElementById("myInput").value;
  var count = 0;
  for (var i = 0; i < listOfStocks.length; i++) {
    if (listOfStocks[i] == nameOfStockSearched) {
      count++;
    }
  }
  if (count != 0) {
    fetch(url + "api/stock-list/nameOfStock=" + nameOfStockSearched)
      .then((response) => response.json())
      .then((stockDetails) => {
            console.log(stockDetails.nameOfStock);
            document.getElementById("nameOfStockFetchedFromSearch").innerHTML =
              stockDetails.nameOfStock;
            document.getElementById("stock-ticker").innerHTML =
              "Stock Ticker: " + stockDetails.stockTicker;
            document.getElementById("stock-price").innerHTML =
              "Stock Price: " + stockDetails.price;
          
      });
    var modal = document.getElementById("myModal");
    modal.style.display = "block";
    
  } else {
    alert("Enter a valid stock");
  }
}

//Nav bar stock search suggestion
function getListOfStocks() {
  listOfStocks=[];
  fetch(url + "api/stock-list/")
    .then((response) => response.json())
    .then((trades) => {
      if (trades.length > 0) {
        trades.forEach((itemData) => {
          listOfStocks.push(itemData.nameOfStock);
        });
      }
    });
  autocomplete(document.getElementById("myInput"), listOfStocks);
}

function autocomplete(inp, arr) {
  var currentFocus;
  inp.addEventListener("input", function (e) {
    var a,
      b,
      i,
      val = this.value;
    closeAllLists();
    if (!val) {
      return false;
    }
    currentFocus = -1;
    a = document.createElement("DIV");
    a.setAttribute("id", this.id + "autocomplete-list");
    a.setAttribute("class", "autocomplete-items");
    this.parentNode.appendChild(a);
    for (i = 0; i < arr.length; i++) {
      if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
        b = document.createElement("DIV");

        b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
        b.innerHTML += arr[i].substr(val.length);

        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";

        b.addEventListener("click", function (e) {
          inp.value = this.getElementsByTagName("input")[0].value;

          closeAllLists();
        });
        a.appendChild(b);
      }
    }
  });

  function closeAllLists(elmnt) {
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }

  document.addEventListener("click", function (e) {
    closeAllLists(e.target);
  });
}

function logout() {
  window.localStorage.removeItem("username");
  window.location.href = "home.html";
}
