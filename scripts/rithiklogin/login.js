var username = window.localStorage.getItem("username");
if(!(username == null))
{
    window.location.href = "portfolio.html";   
}


function login() {
    $("#status").fadeIn(); // will first fade out the loading animation
    $("#preloader").delay(350).fadeIn("slow"); // will fade out the white DIV that covers the website.
    $("body").delay(350).css({
        overflow: "hidden"
    });
    var userEmail = document.getElementById("email").value;
    var userPass = document.getElementById("pass").value;
    if (userEmail == "" || userPass == "") {
        $("#status").fadeOut(); // will first fade out the loading animation
        $("#preloader").delay(350).fadeOut("slow"); // will fade out the white DIV that covers the website.
        $("body").delay(350).css({
            overflow: "visible"
        });
        window.alert("Username or password is empty.");
    } else {
        // window.alert(userEmail+" "+userPass);
        fetch(url+"/api/user-details/"+userEmail+"/"+userPass)
        .then(response => response.json())
        .then(result => {
            console.log(result);
		    if(result.username == userEmail)
            {
                $("#status").fadeOut(); // will first fade out the loading animation
                $("#preloader").delay(350).fadeOut("slow"); // will fade out the white DIV that covers the website.
                $("body").delay(350).css({
                    overflow: "visible"
                });
                window.localStorage.setItem("username", userEmail);
                window.location.href = "portfolio.html";
                window.alert("Login Successful");
            }
            else
            {
                $("#status").fadeOut(); // will first fade out the loading animation
                $("#preloader").delay(350).fadeOut("slow"); // will fade out the white DIV that covers the website.
                $("body").delay(350).css({
                    overflow: "visible"
                });
                window.alert("Error : The email or the password is incorrect");
            }
        }).catch((error) => {
            $("#status").fadeOut(); // will first fade out the loading animation
            $("#preloader").delay(350).fadeOut("slow"); // will fade out the white DIV that covers the website.
            $("body").delay(350).css({
                overflow: "visible"
            });
            console.error('Error:', error);
            window.alert("Error : The email or the password is incorrect");
          });
    }
}
