
var username = window.localStorage.getItem("username");
//Main function
function tradeBookTablePopulate() {
  let name = "hi";
  console.log(name);
  fetch(url + "api/stock-trades/username="+username)
    .then((response) => response.json())
    .then((trades) => {
      if (trades.length > 0) {
        var temp = "";
        trades.forEach((itemData) => {
          temp += "<tr>";
          temp += "<td>" + itemData.stockTicker + "</td>";
          temp += "<td>" + itemData.date_time + "</td>";
          temp += "<td>" + itemData.buyOrSell + "</td>";
          temp += "<td>" + itemData.price + "</td>";
          temp += "<td>" + itemData.volume + "</td>";
          temp += "<td>" + itemData.price * itemData.volume + "</td>";
          temp += "<td>" + ((itemData.statusCode == 0) ? "Pending" : (itemData.statusCode == 1) ? "Order Sent to Exchange" : (itemData.statusCode == 2) ? "Success" : "Failed" )+ "</td>";
          temp += "<td><a href='ordernow.html' >Reorder</a></td>";
          temp += "</tr>";
        });
        document.getElementById("tradeBookTableBody").innerHTML = temp;
      }
      
    });
}

//To XL
function ExportToExcel(type, fn, dl) {
  var elt = document.getElementById("sortable");
  var wb = XLSX.utils.table_to_book(elt, { sheet: "sheet1" });
  return dl
    ? XLSX.write(wb, { bookType: type, bookSST: true, type: "base64" })
    : XLSX.writeFile(wb, fn || "MySheetName." + (type || "xlsx"));
}

//To PDF
function ExportToPDF() {
  html2canvas(document.getElementById("sortable"), {
    onrendered: function (canvas) {
      var data = canvas.toDataURL();
      var docDefinition = {
        content: [
          {
            image: data,
            width: 500,
          },
        ],
      };
      pdfMake.createPdf(docDefinition).download("Table.pdf");
    },
  });
}

function sortTable(n, type) {
  var table,
    rows,
    switching,
    i,
    x,
    y,
    shouldSwitch,
    dir,
    switchcount = 0;
  table = document.getElementById("sortable");
  switching = true;

  dir = "asc";
  while (switching) {
    switching = false;
    rows = table.rows;

    for (i = 1; i < rows.length - 1; i++) {
      shouldSwitch = false;

      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];

      if (dir == "asc") {
        if (type == "str") {
          if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
            shouldSwitch = true;
            break;
          }
        } else if (type == "num") {
          if (parseFloat(x.innerHTML) > parseFloat(y.innerHTML)) {
            shouldSwitch = true;
            break;
          }
        }
      } else if (dir == "desc") {
        if (type == "str") {
          if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
            shouldSwitch = true;
            break;
          }
        } else if (type == "num") {
          if (parseFloat(x.innerHTML) < parseFloat(y.innerHTML)) {
            shouldSwitch = true;
            break;
          }
        }
      }
    }
    if (shouldSwitch) {
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;

      switchcount++;
    } else {
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}

function validateDates(fromDate, toDate) {
  const fDate = new Date(fromDate);
  const tDate = new Date(toDate);
  const curDate = new Date();
  if (fDate < tDate && +fDate <= curDate && +tDate <= curDate) {
    return true;
  }
  return false;
}

function getBetweenDates() {
  let fromDate = document.getElementById("fromDt").value;
  let toDate = document.getElementById("toDt").value;
  let check = validateDates(fromDate, toDate);

  if (check == true) {
    console.log(fromDate);
    fetch(url + "api/stock-trades/"+username+"/" + fromDate + "/" + toDate)
      .then((response) => response.json())
      .then((trades) => {
        if (trades.length > 0) {
          var temp = "";
          trades.forEach((itemData) => {
            temp += "<tr>";
            temp += "<td>" + itemData.nameOfStock + "</td>";
            temp += "<td>" + itemData.date_time + "</td>";
            temp += "<td>" + itemData.buyOrSell + "</td>";
            temp += "<td>" + itemData.price + "</td>";
            temp += "<td>" + itemData.volume + "</td>";
            temp += "<td>" + itemData.price * itemData.volume + "</td>";
            temp += "<td>" + itemData.statusCode + "</td>";
            temp += "<td><a href='ordernow.html'>Reorder</a></td>";
            temp += "</tr>";
          });
          document.getElementById("tradeBookTableBody").innerHTML = temp;
        }
        else{
          var tmp = "";
          tmp += "<tr><td colspan='8'>No stocks found in the range "+fromDate+" to "+toDate+"</td></tr>";
          document.getElementById("tradeBookTableBody").innerHTML = tmp;
        }
      });
  } else {
    alert("Date range invalid");
  }
}
